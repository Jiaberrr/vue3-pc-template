
const routes = [
    {
        path: '/cardTemplate',
        id: 1000,
        name: 'cardTemplate',
        meta: {
            title: '卡片合集'
        },
        children: [{
            path: 'cardTemplate1',
            id: 1001,
            name: 'cardTemplate1',
            meta: {
                title: '普通卡片',
            }
        },
        {
            path: 'cardTemplate3',
            id: 1002,
            name: 'cardTemplate3',
            meta: {
                title: '可拖拽卡片',
            }
        },
        {
            path: 'cardTemplate4',
            id: 1003,
            name: 'cardTemplate4',
            meta: {
                title: '可缩放卡片',
            }
        },
        {
            path: 'cardTemplate2',
            id: 1004,
            name: 'cardTemplate2',
            meta: {
                title: 'echarts卡片'
            }
        }
        ]
    },
    {
        path: '/formTemplate',
        id: 2000,
        name: 'formTemplate',
        meta: {
            title: '表单示例'
        },
        children: [{
            path: 'formTemplate1',
            id: 2001,
            name: 'formTemplate1',
            meta: {
                title: '示例1',
            }
        },
        {
            path: 'formTemplate2',
            id: 2002,
            name: 'formTemplate2',
            meta: {
                title: '示例2'
            }
        }
        ]
    },
    {
        path: '/tableTemplate',
        id: 3000,
        name: 'tableTemplate',
        meta: {
            title: '表格示例'
        },
        children: [{
            path: 'tableTemplate1',
            id: 3001,
            name: 'tableTemplate1',
            meta: {
                title: '示例1',
            }
        },
        {
            path: 'tableTemplate2',
            id: 3002,
            name: 'tableTemplate2',
            meta: {
                title: '示例2',
            }
        }
        ]
    },
    {
        path: '/pdfTemplate',
        id: 5000,
        name: 'pdfTemplate',
        meta: {
            title: 'pdf示例'
        },
        children: [{
            path: 'pdfTemplate1',
            id: 5001,
            name: 'pdfTemplate1',
            meta: {
                title: '预览PDF1',
            }
        },
        {
            path: 'pdfTemplate2',
            id: 5002,
            name: 'pdfTemplate2',
            meta: {
                title: '预览PDF2',
            }
        }
        ]
    },
    {
        path: '/systems',
        id: 4000,
        name: 'systems',
        meta: {
            title: '系统设置'
        },
        children: [{
            path: 'structures',
            id: 4001,
            name: 'structures',
            meta: {
                title: '组织架构',
            }
        }
        ]
    }
]

// 模拟获取动态路由数据
export function getDynamicRoutes(){
    return new Promise((resolve) => {
        resolve(routes)
    })
}